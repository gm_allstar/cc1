package cc1;
//import java.awt.Point;
import java.io.*;
import java.util.ArrayList;
import java.util.regex.*;

/**
 *
 * @author Jefferey Ostapchuk
 */
public class CC1 {
    private static Integer rw, rd;
    private static Integer bx, by;
    private static Integer cleaned = 0;
    private static String path;
    private static ArrayList<String> dirt = new ArrayList<>();
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        cleaned = 0;
//        rw = 5; rd = 5;
//        bx = 1; by = 2;
//        dirt = new ArrayList();
//        dirt.add("1 0");
//        dirt.add("2 2");
//        dirt.add("2 3");
//        path = "NNESEESWNWW";
        try {
            ArrayList<String> input = new ArrayList<>();
            FileReader reader = new FileReader("input.txt");
            //LineNumberReader r = new LineNumberReader(reader);
            BufferedReader r = new BufferedReader(reader);
            
            String line;
            while((line = r.readLine()) != null)
                input.add(line);
            
            if(input.size() < 3)
                throw new Exception("Not enough data to execute!");

            path = input.remove(input.size() - 1); // last index must be path
            if(!path.matches("^[NSEW]+$"))
                throw new Exception("Invalid syntax in input.");
            
            
            Pattern regex = Pattern.compile("^(\\d+) (\\d+)$", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
            Matcher match = null;
            
            // get room w and d
            match = regex.matcher(input.remove(0));
            if (!match.find())
                throw new Exception("Invalid syntax in input.");
            
            rw = Integer.parseInt(match.group(1));
            rd = Integer.parseInt(match.group(2));
            
            // get robot x and y
            match = regex.matcher(input.remove(0));
            if (!match.find())
                throw new Exception("Invalid syntax in input.");
            
            bx = Integer.parseInt(match.group(1));
            by = Integer.parseInt(match.group(2));
            
            for(int i = input.size(); i > 0; i--) {
                String point = input.remove(0);
                if (!point.matches("^(\\d+) (\\d+)$"))
                    throw new Exception("Invalid syntax in input.");
                
                dirt.add(point);//String.format("%1$d %2$d", match.group(1), match.group(2)));
            }
            
            for(char d : path.toCharArray()) {
                if((d == 'N') && (by < rd))
                    by++;

                if((d == 'S') && (by > 0))
                    by--;

                if((d == 'E') && (bx < rw))
                    bx++;

                if((d == 'W') && (bx > 0))
                    bx--;
                
                if(dirt.remove(String.format("%1$d %2$d", bx, by))) // i clean :)
                    cleaned++;

            }
            System.out.println(String.format("%1$d %2$d", bx, by));
            System.out.println(String.format("%1$d dirt spot(s) cleaned!", cleaned));
        } catch(IOException ex) {
            System.out.println("Error in reading input.txt");
        } catch(PatternSyntaxException ex) {
            System.out.println("Syntax error in input.");
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
